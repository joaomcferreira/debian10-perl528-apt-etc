FROM registry.gitlab.com/joaomcferreira/debian10-perl528-apt:181878133
#ROM registry.gitlab.com/joaomcferreira/debian10-perl528-apt:181855232

COPY README.md /force_rebuild_1603736150.txt

MAINTAINER João Miguel Ferreira <joao.miguel.c.ferreira@gmail.com>

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update
RUN apt upgrade -y

#
#usefull things
RUN apt install -y curl

#
#Mr. V. needs these two
RUN apt install -y awscli docker.io

#
#cpanminus, make and gcc for additional Mr. V. things not availabe through apt (Paws, f.ex.)
RUN apt install -y cpanminus make
ENV CPANM_OPTS_V_ID="--installdeps --no-man-pages"
COPY cpanfiles/n1/cpanfile /build1/
RUN cpanm $CPANM_OPTS_V_ID /build1/ && rm -rf /root/.cpanm
RUN apt install -y gcc
COPY cpanfiles/n2/cpanfile /build2/
RUN cpanm $CPANM_OPTS_V_ID /build2/ && rm -rf /root/.cpanm
COPY cpanfiles/n3/cpanfile /build3/
RUN cpanm $CPANM_OPTS_V_ID /build3/ && rm -rf /root/.cpanm

#
#Mr. V. needs this one too
RUN apt install -y gnuplot

#
#Mr. V. also needs Kafka::Librd
COPY kafka_apts.txt /build/
RUN apt install -y $(cat /build/kafka_apts.txt)
RUN cpanm Kafka::Librd && rm -rf /root/.cpanm

COPY . /app
WORKDIR /app

CMD ["prove"]

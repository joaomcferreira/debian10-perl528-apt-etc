#!/usr/bin/env perl
use strict;
use warnings;

use Test::More;

my $d = `docker -v`;
print STDERR $d;
like $d, qr/Docker/, 'check that docker exists on the command line';

my $a = `aws --version`;
print STDERR $a;
like $a, qr/aws-cli/, 'check that aws exists on the command line';

my $g = `gnuplot --version`;
print STDERR $g;
like $g, qr/gnuplot/, 'check that gnuplot exists on the command line';

done_testing();
